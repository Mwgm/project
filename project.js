function buttonFunction(elem){
    var x = elem.id;

    switch(x){
        case "bHome":
            hide("CV");
            hide("background");
            hide("skills")
            hide("work")
            hide("contact")
            hide("project")
            break;
        case "bCV":
            displayHide("CV")
            hide("background")
            hide("skills")
            hide("work")
            hide("contact")
            hide("project")
            break;
        case "bBackground":
            displayHide("background")
            hide("CV")
            hide("skills")
            hide("work")
            hide("contact")
            hide("project")
            break;
        case "bSkills":
            displayHide("skills")
            hide("CV")
            hide("background")
            hide("work")
            hide("contact")
            hide("project")
            break;
        case "bProject":
            displayHide("project")
            hide("CV");
            hide("background");
            hide("skills")
            hide("work")
            hide("contact")
            break;
        case "bWork":
            displayHide("work")
            hide("CV")
            hide("background")
            hide("skills")
            hide("contact")
            hide("project")
            break;
        case "bContact":
            displayHide("contact")
            hide("CV")
            hide("background")
            hide("skills")
            hide("work")
            hide("project")
            break;
        default:
            return false;
    }
}

function displayHide(elem){
    var temp = document.getElementById(elem);
    if(temp.style.display === "none"){
        temp.style.display = "block";
    }
    else{
        temp.style.display = "none";
    }
}

function  hide(elem){
    var temp = document.getElementById(elem);
    temp.style.display = "none";
}
